package br.ifsc.edu.sorteio;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    EditText A,B;
    TextView resultado;
    String valorA, valorB;
    int valorFA, valorFB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        A = findViewById(R.id.A);
        B = findViewById(R.id.B);
        resultado = findViewById(R.id.resultado);
    }
    public void sortear(View v){
        Random r = new Random();

        valorA = A.getText().toString().trim();
        valorB = B.getText().toString().trim();

        if (valorA.equals("") || valorB.equals("")){
            Toast.makeText(getApplicationContext(), "Caixa de texto vazia", Toast.LENGTH_LONG).show();
        }

        else{
            valorFA=Integer.parseInt(valorA);
            valorFB=Integer.parseInt(valorB);

            if (valorFA>valorFB){
                int n = r.nextInt((valorFA-valorFB)+1) + valorFB;
                String mySting = String.valueOf(n);
                resultado.setText(mySting);
            }

            else if (valorFB>valorFA){
                int n = r.nextInt((valorFB-valorFA)+1) + valorFA;
                String mySting = String.valueOf(n);
                resultado.setText(mySting);
            }
            else if (valorFA==valorFB){
                Toast.makeText(getApplicationContext(), "Números iguais. Tente novamente", Toast.LENGTH_LONG).show();
            }

        }

    }
}
